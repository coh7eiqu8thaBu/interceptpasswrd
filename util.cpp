/********************************************************************
** This file is part of 'AcctSync' package.
**
**  AcctSync is free software; you can redistribute it and/or modify
**  it under the terms of the Lesser GNU General Public License as
**  published by the Free Software Foundation; either version 2
**  of the License, or (at your option) any later version.
**
**  AcctSync is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  Lesser GNU General Public License for more details.
**
**  You should have received a copy of the Lesser GNU General Public
**  License along with AcctSync; if not, write to the
**	Free Software Foundation, Inc.,
**	59 Temple Place, Suite 330,
**	Boston, MA  02111-1307
**	USA
**
** +AcctSync was originally Written by.
**  Kervin Pierre
**  Information Technology Department
**  Florida Tech
**  MAR, 2002
**
** +Modified by.
**  Brian Clayton
**  Information Technology Services
**  Clark University
**  MAR, 2008
**  OCT, 2010 - added alloc_copy function
**  JAN, 2011 - removed non-Unicode code, fixed memory leaks introduced by Unicode support code, handled NULL pointers
**  MAR, 2011 - fixed careless typos, added memory wiping to rawurlencode and doublequote functions
**  SEP, 2017 - fixed minor naming inconsistencies
** Redistributed under the terms of the LGPL
** license.  See LICENSE.txt file included in
** this package for details.
**
********************************************************************/

#include "passwdhk.h"
//#include "fixedint.h"


/* LibTomCrypt, modular cryptographic library -- Tom St Denis
*
* LibTomCrypt is a library that provides various cryptographic
* algorithms in a highly modular and flexible manner.
*
* The library is free for all purposes without any express
* guarantee it works.
*
* Tom St Denis, tomstdenis@gmail.com, http://libtom.org
*/

#include "sha512.h"

/* the K array */
static const uint64_t K[80] = {
	UINT64_C(0x428a2f98d728ae22), UINT64_C(0x7137449123ef65cd),
	UINT64_C(0xb5c0fbcfec4d3b2f), UINT64_C(0xe9b5dba58189dbbc),
	UINT64_C(0x3956c25bf348b538), UINT64_C(0x59f111f1b605d019),
	UINT64_C(0x923f82a4af194f9b), UINT64_C(0xab1c5ed5da6d8118),
	UINT64_C(0xd807aa98a3030242), UINT64_C(0x12835b0145706fbe),
	UINT64_C(0x243185be4ee4b28c), UINT64_C(0x550c7dc3d5ffb4e2),
	UINT64_C(0x72be5d74f27b896f), UINT64_C(0x80deb1fe3b1696b1),
	UINT64_C(0x9bdc06a725c71235), UINT64_C(0xc19bf174cf692694),
	UINT64_C(0xe49b69c19ef14ad2), UINT64_C(0xefbe4786384f25e3),
	UINT64_C(0x0fc19dc68b8cd5b5), UINT64_C(0x240ca1cc77ac9c65),
	UINT64_C(0x2de92c6f592b0275), UINT64_C(0x4a7484aa6ea6e483),
	UINT64_C(0x5cb0a9dcbd41fbd4), UINT64_C(0x76f988da831153b5),
	UINT64_C(0x983e5152ee66dfab), UINT64_C(0xa831c66d2db43210),
	UINT64_C(0xb00327c898fb213f), UINT64_C(0xbf597fc7beef0ee4),
	UINT64_C(0xc6e00bf33da88fc2), UINT64_C(0xd5a79147930aa725),
	UINT64_C(0x06ca6351e003826f), UINT64_C(0x142929670a0e6e70),
	UINT64_C(0x27b70a8546d22ffc), UINT64_C(0x2e1b21385c26c926),
	UINT64_C(0x4d2c6dfc5ac42aed), UINT64_C(0x53380d139d95b3df),
	UINT64_C(0x650a73548baf63de), UINT64_C(0x766a0abb3c77b2a8),
	UINT64_C(0x81c2c92e47edaee6), UINT64_C(0x92722c851482353b),
	UINT64_C(0xa2bfe8a14cf10364), UINT64_C(0xa81a664bbc423001),
	UINT64_C(0xc24b8b70d0f89791), UINT64_C(0xc76c51a30654be30),
	UINT64_C(0xd192e819d6ef5218), UINT64_C(0xd69906245565a910),
	UINT64_C(0xf40e35855771202a), UINT64_C(0x106aa07032bbd1b8),
	UINT64_C(0x19a4c116b8d2d0c8), UINT64_C(0x1e376c085141ab53),
	UINT64_C(0x2748774cdf8eeb99), UINT64_C(0x34b0bcb5e19b48a8),
	UINT64_C(0x391c0cb3c5c95a63), UINT64_C(0x4ed8aa4ae3418acb),
	UINT64_C(0x5b9cca4f7763e373), UINT64_C(0x682e6ff3d6b2b8a3),
	UINT64_C(0x748f82ee5defb2fc), UINT64_C(0x78a5636f43172f60),
	UINT64_C(0x84c87814a1f0ab72), UINT64_C(0x8cc702081a6439ec),
	UINT64_C(0x90befffa23631e28), UINT64_C(0xa4506cebde82bde9),
	UINT64_C(0xbef9a3f7b2c67915), UINT64_C(0xc67178f2e372532b),
	UINT64_C(0xca273eceea26619c), UINT64_C(0xd186b8c721c0c207),
	UINT64_C(0xeada7dd6cde0eb1e), UINT64_C(0xf57d4f7fee6ed178),
	UINT64_C(0x06f067aa72176fba), UINT64_C(0x0a637dc5a2c898a6),
	UINT64_C(0x113f9804bef90dae), UINT64_C(0x1b710b35131c471b),
	UINT64_C(0x28db77f523047d84), UINT64_C(0x32caab7b40c72493),
	UINT64_C(0x3c9ebe0a15c9bebc), UINT64_C(0x431d67c49c100d4c),
	UINT64_C(0x4cc5d4becb3e42b6), UINT64_C(0x597f299cfc657e2a),
	UINT64_C(0x5fcb6fab3ad6faec), UINT64_C(0x6c44198c4a475817)
};

/* Various logical functions */

#define ROR64c(x, y) \
	( ((((x)&UINT64_C(0xFFFFFFFFFFFFFFFF))>>((uint64_t)(y)&UINT64_C(63))) | \
	  ((x)<<((uint64_t)(64-((y)&UINT64_C(63)))))) & UINT64_C(0xFFFFFFFFFFFFFFFF))

#define STORE64H(x, y)                                                                     \
   { (y)[0] = (unsigned char)(((x)>>56)&255); (y)[1] = (unsigned char)(((x)>>48)&255);     \
	 (y)[2] = (unsigned char)(((x)>>40)&255); (y)[3] = (unsigned char)(((x)>>32)&255);     \
	 (y)[4] = (unsigned char)(((x)>>24)&255); (y)[5] = (unsigned char)(((x)>>16)&255);     \
	 (y)[6] = (unsigned char)(((x)>>8)&255); (y)[7] = (unsigned char)((x)&255); }

#define LOAD64H(x, y)                                                      \
   { x = (((uint64_t)((y)[0] & 255))<<56)|(((uint64_t)((y)[1] & 255))<<48) | \
		 (((uint64_t)((y)[2] & 255))<<40)|(((uint64_t)((y)[3] & 255))<<32) | \
		 (((uint64_t)((y)[4] & 255))<<24)|(((uint64_t)((y)[5] & 255))<<16) | \
		 (((uint64_t)((y)[6] & 255))<<8)|(((uint64_t)((y)[7] & 255))); }

#define Ch(x,y,z)       (z ^ (x & (y ^ z)))
#define Maj(x,y,z)      (((x | y) & z) | (x & y))
#define S(x, n)         ROR64c(x, n)
#define R(x, n)         (((x) &UINT64_C(0xFFFFFFFFFFFFFFFF))>>((uint64_t)n))
#define Sigma0(x)       (S(x, 28) ^ S(x, 34) ^ S(x, 39))
#define Sigma1(x)       (S(x, 14) ^ S(x, 18) ^ S(x, 41))
#define Gamma0(x)       (S(x, 1) ^ S(x, 8) ^ R(x, 7))
#define Gamma1(x)       (S(x, 19) ^ S(x, 61) ^ R(x, 6))
#ifndef MIN
#define MIN(x, y) ( ((x)<(y))?(x):(y) )
#endif

/* compress 1024-bits */
#define NEED_SMALL_CODE
static int sha512_compress(sha512_context *md, unsigned char *buf) {
	uint64_t S[8], W[80], t0, t1;
	int i;

	/* copy state into S */
	for (i = 0; i < 8; i++) {
		S[i] = md->state[i];
	}

	/* copy the state into 1024-bits into W[0..15] */
	for (i = 0; i < 16; i++) {
		LOAD64H(W[i], buf + (8 * i));
	}

	/* fill W[16..79] */
	for (i = 16; i < 80; i++) {
		W[i] = Gamma1(W[i - 2]) + W[i - 7] + Gamma0(W[i - 15]) + W[i - 16];
	}

	/* Compress */
#ifdef NEED_SMALL_CODE
	for (i = 0; i < 80; i++) {
		t0 = S[7] + Sigma1(S[4]) + Ch(S[4], S[5], S[6]) + K[i] + W[i];
		t1 = Sigma0(S[0]) + Maj(S[0], S[1], S[2]);
		S[7] = S[6];
		S[6] = S[5];
		S[5] = S[4];
		S[4] = S[3] + t0;
		S[3] = S[2];
		S[2] = S[1];
		S[1] = S[0];
		S[0] = t0 + t1;
	}
#else
#define RND(a,b,c,d,e,f,g,h,i) \
	t0 = h + Sigma1(e) + Ch(e, f, g) + K[i] + W[i]; \
	t1 = Sigma0(a) + Maj(a, b, c);\
	d += t0; \
	h  = t0 + t1;

	for (i = 0; i < 80; i += 8) {
		RND(S[0], S[1], S[2], S[3], S[4], S[5], S[6], S[7], i + 0);
		RND(S[7], S[0], S[1], S[2], S[3], S[4], S[5], S[6], i + 1);
		RND(S[6], S[7], S[0], S[1], S[2], S[3], S[4], S[5], i + 2);
		RND(S[5], S[6], S[7], S[0], S[1], S[2], S[3], S[4], i + 3);
		RND(S[4], S[5], S[6], S[7], S[0], S[1], S[2], S[3], i + 4);
		RND(S[3], S[4], S[5], S[6], S[7], S[0], S[1], S[2], i + 5);
		RND(S[2], S[3], S[4], S[5], S[6], S[7], S[0], S[1], i + 6);
		RND(S[1], S[2], S[3], S[4], S[5], S[6], S[7], S[0], i + 7);
	}
#undef RND
#endif


	/* feedback */
	for (i = 0; i < 8; i++) {
		md->state[i] = md->state[i] + S[i];
	}

	return 0;
}

/**
Initialize the hash state
@param md   The hash state you wish to initialize
@return 0 if successful
*/
int sha512_init(sha512_context * md) {
	if (md == NULL) return 1;

	md->curlen = 0;
	md->length = 0;
	md->state[0] = UINT64_C(0x6a09e667f3bcc908);
	md->state[1] = UINT64_C(0xbb67ae8584caa73b);
	md->state[2] = UINT64_C(0x3c6ef372fe94f82b);
	md->state[3] = UINT64_C(0xa54ff53a5f1d36f1);
	md->state[4] = UINT64_C(0x510e527fade682d1);
	md->state[5] = UINT64_C(0x9b05688c2b3e6c1f);
	md->state[6] = UINT64_C(0x1f83d9abfb41bd6b);
	md->state[7] = UINT64_C(0x5be0cd19137e2179);

	return 0;
}

/**
Process a block of memory though the hash
@param md     The hash state
@param in     The data to hash
@param inlen  The length of the data (octets)
@return 0 if successful
*/
int sha512_process(sha512_context * md, const unsigned char *in, size_t inlen) {
	size_t n;
	size_t i;
	int           err;
	if (md == NULL) return 1;
	if (in == NULL) return 1;
	if (md->curlen > sizeof(md->buf)) {
		return 1;
	}
	while (inlen > 0) {
		if (md->curlen == 0 && inlen >= 128) {
			if ((err = sha512_compress(md, (unsigned char *)in)) != 0) {
				return err;
			}
			md->length += 128 * 8;
			in += 128;
			inlen -= 128;
		}
		else {
			n = MIN(inlen, (128 - md->curlen));

			for (i = 0; i < n; i++) {
				md->buf[i + md->curlen] = in[i];
			}

			md->curlen += n;
			in += n;
			inlen -= n;
			if (md->curlen == 128) {
				if ((err = sha512_compress(md, md->buf)) != 0) {
					return err;
				}
				md->length += 8 * 128;
				md->curlen = 0;
			}
		}
	}
	return 0;
}

/**
Terminate the hash to get the digest
@param md  The hash state
@param out [out] The destination of the hash (64 bytes)
@return 0 if successful
*/
int sha512_done(sha512_context * md, unsigned char *out) {
	int i;

	if (md == NULL) return 1;
	if (out == NULL) return 1;

	if (md->curlen >= sizeof(md->buf)) {
		return 1;
	}

	/* increase the length of the message */
	md->length += md->curlen * UINT64_C(8);

	/* append the '1' bit */
	md->buf[md->curlen++] = (unsigned char)0x80;

	/* if the length is currently above 112 bytes we append zeros
	* then compress.  Then we can fall back to padding zeros and length
	* encoding like normal.
	*/
	if (md->curlen > 112) {
		while (md->curlen < 128) {
			md->buf[md->curlen++] = (unsigned char)0;
		}
		sha512_compress(md, md->buf);
		md->curlen = 0;
	}

	/* pad upto 120 bytes of zeroes
	* note: that from 112 to 120 is the 64 MSB of the length.  We assume that you won't hash
	* > 2^64 bits of data... :-)
	*/
	while (md->curlen < 120) {
		md->buf[md->curlen++] = (unsigned char)0;
	}

	/* store length */
	STORE64H(md->length, md->buf + 120);
	sha512_compress(md, md->buf);

	/* copy output */
	for (i = 0; i < 8; i++) {
		STORE64H(md->state[i], out + (8 * i));
	}

	/* Clean the stack */
	SecureZeroMemory(md, sizeof(sha512_context));

	return 0;
}

int sha512(const unsigned char *message, size_t message_len, unsigned char *out) {
	sha512_context ctx;
	int ret;
	if ((ret = sha512_init(&ctx))) return ret;
	if ((ret = sha512_process(&ctx, message, message_len))) return ret;
	if ((ret = sha512_done(&ctx, out))) return ret;
	return 0;
}

const char b64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

size_t b64_encoded_size(size_t inlen) {
	size_t ret;

	ret = inlen;
	if (inlen % 3 != 0)
		ret += 3 - (inlen % 3);
	ret /= 3;
	ret *= 4;

	return ret;
}

// Dynamically allocated, needs to be freed
// Encode to Base64
char *b64_encode(const unsigned char *in, size_t len) {
	char   *out;
	size_t  elen;
	size_t  i;
	size_t  j;
	size_t  v;

	if (in == NULL || len == 0)
		return NULL;

	elen = b64_encoded_size(len);
	out = (char *)calloc(elen + 1, sizeof(char));
	out[elen] = '\0';

	for (i = 0, j = 0; i < len; i += 3, j += 4) {
		v = in[i];
		v = i + 1 < len ? v << 8 | in[i + 1] : v << 8;
		v = i + 2 < len ? v << 8 | in[i + 2] : v << 8;

		out[j] = b64chars[(v >> 18) & 0x3F];
		out[j + 1] = b64chars[(v >> 12) & 0x3F];
		if (i + 1 < len) {
			out[j + 2] = b64chars[(v >> 6) & 0x3F];
		}
		else {
			out[j + 2] = '=';
		}
		if (i + 2 < len) {
			out[j + 3] = b64chars[v & 0x3F];
		}
		else {
			out[j + 3] = '=';
		}
	}
	out[elen] = '\0';
	return out;
}


// Dynamically allocated, needs to be freed
// Convert "len" byte to HEX
char *HEXDump(char *membuff, unsigned int len) {
	char *out;
	unsigned int i, ascii_length;

	ascii_length = len << 1;
	out = (char *)calloc(ascii_length + 1, sizeof(char));
	if (out == NULL)
		return NULL;

	for (i = 0; i < len; i++) {
		_snprintf_s(out + i, 2, 2, "%X", (membuff[i] & 0xFF));
	}

	out[ascii_length] = 0x00;
	return out;
}


int gettimeofday(struct timeval * tp, struct timezone * tzp) {
	// Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
	// This magic number is the number of 100 nanosecond intervals since January 1, 1601 (UTC)
	// until 00:00:00 January 1, 1970 
	static const uint64_t EPOCH = ((uint64_t)116444736000000000ULL);

	SYSTEMTIME  system_time;
	FILETIME    file_time;
	uint64_t    time;

	GetSystemTime(&system_time);
	SystemTimeToFileTime(&system_time, &file_time);
	time = ((uint64_t)file_time.dwLowDateTime);
	time += ((uint64_t)file_time.dwHighDateTime) << 32;

	tp->tv_sec = (long)((time - EPOCH) / 10000000L);
	tp->tv_usec = (long)(system_time.wMilliseconds * 1000);
	return 0;
}

extern pshkConfigStruct pshk_config;
extern HANDLE hExecProgMutex;

LPWSTR bool_to_string(BOOL b) {
	return (b ? L"true" : L"false");
}

LPWSTR null_to_string(LPWSTR s) {
	return (s == NULL ? L"NULL" : s);
}

// Dynamically allocated, needs to be freed
LPWSTR alloc_copy(LPWSTR src, size_t length) {
	WCHAR *ret = (WCHAR *)calloc(1, length + sizeof(wchar_t));
	if (ret != NULL) memcpy(ret, src, length);
	return ret;
}

// Dynamically allocated, needs to be freed
LPWSTR pshk_struct2str() {
	wchar_t *tmp, *ret;

	tmp = (wchar_t *)calloc(1, 10 * PSHK_REG_VALUE_MAX_LEN_BYTES);
	if (tmp != NULL) {
		swprintf_s(tmp, 10 * PSHK_REG_VALUE_MAX_LEN, L"\r\nvalid: %d\r\n---\r\n\
preChangeAction: %s\r\npreChangeProg: '%s'\r\npreChangeProgArgs: '%s'\r\npreChangeProgWait: %d\r\npreChangeProgSkipComp: '%s'\r\n---\r\n\
postChangeAction: %s\r\npostChangeProg: '%s'\r\npostChangeProgArgs: '%s'\r\npostChangeProgWait: %d\r\npostChangeProgSkipComp: '%s'\r\n---\r\n\
logFile: '%s'\r\nmaxLogSize: %d\r\nlogLevel: %d\r\nurlencode: %s\r\ndoublequote: %s\r\nenvironmentStr: '%s'\r\nworkingdirectory: '%s'\r\npriority: %d\r\ninheritParentHandles: %s\r\noutputSSHA512: %s\r\n", \
pshk_config.valid,
bool_to_string(pshk_config.preChangeAction), null_to_string(pshk_config.preChangeProg), null_to_string(pshk_config.preChangeProgArgs), pshk_config.preChangeProgWait, bool_to_string(pshk_config.preChangeProgSkipComp), \
bool_to_string(pshk_config.postChangeAction), null_to_string(pshk_config.postChangeProg), null_to_string(pshk_config.postChangeProgArgs), pshk_config.postChangeProgWait, bool_to_string(pshk_config.postChangeProgSkipComp), \
null_to_string(pshk_config.logFile), pshk_config.maxLogSize, \
pshk_config.logLevel, bool_to_string(pshk_config.urlencode), bool_to_string(pshk_config.doublequote), null_to_string(pshk_config.environmentStr), \
null_to_string(pshk_config.workingDir), pshk_config.priority, bool_to_string(pshk_config.inheritParentHandles), bool_to_string(pshk_config.outputSSHA512) \
);
		ret = _wcsdup(tmp);
		free(tmp);
	}
	else
		ret = _wcsdup(L"pshk_struct2str: cannot allocate memory!"); // _wcsdup needed for consistency so it can be freed

	return ret;
}

// Converts a Unicode string (UTF-16) to UTF-8, URL encodes it, and converts it back
//
// urlencodes a string according to the PHP function rawurlencode
//
// From the PHP manual...
//      Returns a string in which all non-alphanumeric characters
//      except -_. have been replaced with a percent (%) sign
//      followed by two hex digits.
//
// Dynamically allocated, needs to be freed
LPWSTR rawurlencode(LPWSTR src) {
	int size;
	char *utf8Copy;
	LPSTR encodedCopy;
	WCHAR *ret = NULL;
	unsigned int i, j = 0;
	UINT_PTR utf8Len = 0;
	unsigned char c;

	// Get buffer size needed for UTF-8 string
	size = WideCharToMultiByte(CP_UTF8, 0, src, -1, NULL, 0, NULL, NULL);
	if (size != 0) {
		// Allocate and convert
		utf8Copy = (char *)calloc(size, 1);
		if (utf8Copy != NULL) {
			size = WideCharToMultiByte(CP_UTF8, 0, src, -1, utf8Copy, size, NULL, NULL);
			if (size != 0) {
				// URL encode
				utf8Len = strlen(utf8Copy);
				encodedCopy = (LPSTR)calloc(utf8Len + 1, 3);
				if (encodedCopy != NULL) {
					for (i = 0; i < utf8Len; i++) {
						c = (unsigned char)utf8Copy[i]; // Needs to be treated as unsigned for UTF-8
						if (isalnum(c) || c == '-' || c == '_' || c == '.')
							encodedCopy[j++] = c;
						else {
							_snprintf_s(&encodedCopy[j], 4, 3, "%%%2x", c);
							j += 3;
						}
					}

					// Get required buffer size
					size = MultiByteToWideChar(CP_UTF8, 0, encodedCopy, -1, NULL, 0);
					if (size != 0) {
						// Allocate and convert
						ret = (WCHAR *)calloc(size, sizeof(WCHAR));
						if (ret != NULL) size = MultiByteToWideChar(CP_UTF8, 0, encodedCopy, -1, ret, size);
					}
					SecureZeroMemory(encodedCopy, utf8Len * 3);
					free(encodedCopy);
				}
			}
			SecureZeroMemory(utf8Copy, utf8Len);
			free(utf8Copy);
		}
	}
	return ret;
}

// Encloses string in double quotes, escaping any double quotes and backslashes with backslashes
// Dynamically allocated, must be freed
LPWSTR doublequote(LPWSTR src) {
	size_t srcLen = wcslen(src);
	size_t quotedLen = srcLen * 2 + 3; // Room for escaping, beginning/end quotes, and terminating NULL
	wchar_t *quotedCopy = (wchar_t *)calloc(quotedLen, sizeof(wchar_t));
	LPWSTR ret;
	unsigned i, j, k;
	j = 0;
	if (quotedCopy != NULL) {
		quotedCopy[j++] = L'"';
		for (i = 0; i < srcLen; i++) {
			if (src[i] == L'"') {
				k = i;
				while (k > 0 && src[--k] == '\\') // Any backslash or sequence of backslashes immediately preceding the quote must be escaped too
					quotedCopy[j++] = '\\';
				quotedCopy[j++] = '\\';
			}
			quotedCopy[j++] = src[i];
		}
		k = i;
		while (k > 0 && src[--k] == '\\') // Any backslash or sequence of backslashes immediately preceding the closing quote must be escaped too
			quotedCopy[j++] = '\\';
		quotedCopy[j] = L'"';
		ret = _wcsdup(quotedCopy); // Duplicate to eliminate unneeded space
		SecureZeroMemory(quotedCopy, quotedLen * sizeof(wchar_t));
		free(quotedCopy);
	}
	else
		ret = NULL;
	return ret;
}

enum eAccountType {
	atUnknown, atUser, atComputer
};

// Identify if an account is ended by a $, so it's a computer ID
eAccountType GetAccountType(const wchar_t *UserName, int Length) {
	return ((UserName) && (Length > 0))
		? ((UserName[Length - 1] == L'$') ? atComputer : atUser)
		: atUnknown;
}

eAccountType GetAccountType(const UNICODE_STRING &UserName) {
	// UNICODE_STRING::Length is expressed in bytes, not in characters...
	return GetAccountType(UserName.Buffer, UserName.Length / sizeof(wchar_t));
}

// Calls the needed program with supplied user.
//
int pshk_exec_prog(int option, PUNICODE_STRING username, PUNICODE_STRING password) {
	wchar_t *buffer, *prog, *args;
	wchar_t *usernameCopy;
#ifndef RAW_USERNAME
	wchar_t *usernameCopy2;
#endif

	unsigned char sha512out[SHA512SIZE + 1]; // 64 bytes + null byte for saving the SHA512 in binary
	unsigned char salt[SALTSIZE + 1]; // 8 bytes + null byte for the salt in binary
//#ifndef UNICODEPASSWORDTOUTF8
	char CopyOfPasswordInByteAndSalt[PASSWORDSIZE + SALTSIZE + 1]; // 512 byte + salt size + null byte for the copy of the password joined with the salt in binary
#//else
//	char *CopyOfPasswordInByteAndSalt;
//#endif
	char *sha512outB64;
	char *membuff; // Dynamic allocation
	unsigned char SaltedSHA512Output[SSHA512SIZE + 2]; // Final result of SSHA512 in HEX
	unsigned int PasswordSizeInBytes = 0;
	struct timeval time_now {
	};
#ifdef UNICODEPASSWORDTOUTF8
	unsigned int UTF8_PasswordSize = 0;
#endif // UNICODEPASSWORDTOUTF8
	wchar_t *buffer_sprintf; //Dynamic allocation
	int result_of_snprintf = 0;
	wchar_t *passwordCopy, *passwordCopy2;
	wchar_t *bufferFormat = L"\"%s\" %s %s %s";
	int wait = 15000;
	BOOL skipComp = TRUE, outputSSHA512 = TRUE;
	BOOL preChangeAction = FALSE, postChangeAction = FALSE;
	int ret = PSHK_SUCCESS;
	DWORD_PTR bufferChars, bufferBytes;
	DWORD exitCode = 0;
	DWORD progRet;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	SECURITY_ATTRIBUTES sa;
	HANDLE h;

	if (option == PSHK_PRE_CHANGE) {
		preChangeAction = pshk_config.preChangeAction;
		if (preChangeAction == TRUE) {
			prog = pshk_config.preChangeProg;
			args = pshk_config.preChangeProgArgs;
			wait = pshk_config.preChangeProgWait;
			skipComp = pshk_config.preChangeProgSkipComp;
			outputSSHA512 = pshk_config.outputSSHA512;
		}
	}
	else if (option == PSHK_POST_CHANGE) {
		postChangeAction = pshk_config.postChangeAction;
		if (postChangeAction == TRUE) {
			prog = pshk_config.postChangeProg;
			args = pshk_config.postChangeProgArgs;
			wait = pshk_config.postChangeProgWait;
			skipComp = pshk_config.postChangeProgSkipComp;
			outputSSHA512 = pshk_config.outputSSHA512;
		}
	}
	else // Unknown option
		return PSHK_FAILURE;

	// Don't run code if not necessary
	if ((option == PSHK_PRE_CHANGE && preChangeAction == FALSE) || (option == PSHK_POST_CHANGE && postChangeAction == FALSE))
		return PSHK_SUCCESS;

	// If no command is specified, say that we succeeded
	if (wcslen(prog) == 0 && wcslen(args) == 0)
		return PSHK_SUCCESS;

	// If it's a computer return SUCCESS
	if ((skipComp) && (username)) {
		if (GetAccountType(*username) != atUser)
			return PSHK_SUCCESS;
	}

	// Get mutex - unfortunately, this whole section must be mutually exclusive so that the log doesn't get garbled by overlapping writes from multiple threads
	// ** Must be released before return!
	if (WaitForSingleObject(hExecProgMutex, 5000) != 0) {
		// Open log
		h = pshk_log_open();
		pshk_log_write_a(h, "Error Mutex was locked for too long time");
		// Close log
		pshk_log_close(h);
		ret = PSHK_FAILURE;
	}
	else {
		// Open log
		h = pshk_log_open();

		// Copy buffers to ensure null-termination
		usernameCopy = alloc_copy(username->Buffer, username->Length);
		if (outputSSHA512 == TRUE) {
			if (password->Length < (SSHA512SIZE * sizeof(wchar_t))) {
				// Allocate and copy the password if it size is less than the size of SSHA512 hash
				if (pshk_config.logLevel >= PSHK_LOG_DEBUG) {
					pshk_log_write_a(h, "Warning password size is less than SSHA512 hash size");
				}
				// Allocate the needed size for the SSHA512 hash and copy only the right data
				passwordCopy = (WCHAR *)calloc(SSHA512SIZE + 1, sizeof(wchar_t));
				if (passwordCopy != NULL)
					memcpy(passwordCopy, password->Buffer, password->Length);
			}
			else {
				// Allocate and copy the password if it size is bigger than the size of SSHA512 hash
				passwordCopy = alloc_copy(password->Buffer, password->Length);
			}

		}
		else {
			passwordCopy = alloc_copy(password->Buffer, password->Length);
		}


		if (usernameCopy != NULL && passwordCopy != NULL) {
			if (outputSSHA512 == TRUE) {			/*
			We need to generate a salted SHA512 of the password. We use a salt of 8 byte joined at the end of the password.
			The generated output is :
			"{SSHA512}" + BASE64(SHA512(passwordCopy + random Salt) + random Salt)
			*/
				if (pshk_config.logLevel >= PSHK_LOG_DEBUG) {
					pshk_log_write_a(h, "Begin HASH code version :");
					pshk_log_write_a(h, __TIMESTAMP__);
				}

				buffer_sprintf = (wchar_t *)calloc(sizeof(wchar_t), MYBIGBUFFSIZE + 2);
				if (buffer_sprintf == NULL)
					pshk_log_write_a(h, "Error : allocation memory for buffer_sprintf");

				// Initialize the random number generator
				gettimeofday(&time_now, nullptr);
				time_t msecs_time = (time_now.tv_sec * 1000) + (time_now.tv_usec / 1000);
				srand((unsigned int)msecs_time);

				// Determine the length of the password (number of characters, not number of bytes)
				PasswordSizeInBytes = wcslen(passwordCopy); // warning C4267: '=': conversion from 'size_t' to 'unsigned int', possible loss of data
				if (((password->Length >> 1) != PasswordSizeInBytes) && (pshk_config.logLevel >= PSHK_LOG_ERROR)) {
					_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"ERROR will occur because of half of password size (%d/2) and size in byte (%d) is different", password->Length, PasswordSizeInBytes);
					pshk_log_write_w(h, buffer_sprintf);
				}

				if (pshk_config.logLevel >= PSHK_LOG_ALL) {
					_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"DEBUG : convert to (char *) de %d octets", PasswordSizeInBytes);
					pshk_log_write_w(h, buffer_sprintf);
					_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"DEBUG : begin of the received password = %02hhX%02hhX %02hhX%02hhX %02hhX%02hhX %02hhX%02hhX %02hhX%02hhX %02hhX%02hhX %02hhX%02hhX",
						(char)passwordCopy[0x0], (char)passwordCopy[0x1], (char)passwordCopy[0x2], (char)passwordCopy[0x3], (char)passwordCopy[0x4],
						(char)passwordCopy[0x5], (char)passwordCopy[0x6], (char)passwordCopy[0x7], (char)passwordCopy[0x8], (char)passwordCopy[0x9],
						(char)passwordCopy[0xA], (char)passwordCopy[0xB], (char)passwordCopy[0xC], (char)passwordCopy[0xD]);
					pshk_log_write_w(h, buffer_sprintf);
				}
				// Before ... we don't want buffer overflow
				if (PasswordSizeInBytes > PASSWORDSIZE) {
					PasswordSizeInBytes = PASSWORDSIZE;
					pshk_log_write_a(h, "Warning : Truncated password because of exeded size, hope all still ok");
				}
				// Copy the password in the reserved memory space
#ifndef UNICODEPASSWORDTOUTF8
			// Code not compatible with unicode characters, but probably not necessary
				for (unsigned int i = 0; i < PasswordSizeInBytes; i++) {
					if (((unsigned int)passwordCopy[i] & 0xFFFF00) != 0x00) {
						_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"Warning : A char (at position %d) is encoded with more than one byte : (%04X)", i, (unsigned int)passwordCopy[i]);
						pshk_log_write_w(h, buffer_sprintf);
					}
					CopyOfPasswordInByteAndSalt[i] = (unsigned char)passwordCopy[i];
				}

#else	// UNICODEPASSWORDTOUTF8
				// Copy password from UNICODE to UTF-8

				// Get buffer size needed for UTF-8 string with null byte at end
				UTF8_PasswordSize = WideCharToMultiByte(CP_UTF8, 0, passwordCopy, -1, NULL, 0, NULL, NULL);
				if (pshk_config.logLevel >= PSHK_LOG_ALL) {
					_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"DEBUG: Convert %d characters from UNICODE to UTF8 in %d bytes", PasswordSizeInBytes, UTF8_PasswordSize);
					pshk_log_write_w(h, buffer_sprintf);
				}

				if ((pshk_config.logLevel >= PSHK_LOG_ERROR) && (UTF8_PasswordSize == 0)) {
					_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"Error : Can't convert password from UNICODE to UTF-8. Error code is : %d", GetLastError());
					pshk_log_write_w(h, buffer_sprintf);
				}
				//CopyOfPasswordInByteAndSalt = (char *)calloc(sizeof(wchar_t), UTF8_PasswordSize);
				if (CopyOfPasswordInByteAndSalt == NULL)
					pshk_log_write_a(h, "Error : allocation memory for CopyOfPasswordInByteAndSalt");
				UTF8_PasswordSize = WideCharToMultiByte(CP_UTF8, 0, passwordCopy, -1, CopyOfPasswordInByteAndSalt, UTF8_PasswordSize, NULL, NULL) - 1;  // do not count the terminating null character
				CopyOfPasswordInByteAndSalt[UTF8_PasswordSize] = 0x00; // be certain to finish with a null byte

				if (pshk_config.logLevel >= PSHK_LOG_ALL) {
					_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"DEBUG: Converted %d characters from UNICODE to UTF8 in %d bytes + 1 null byte : %02hhX", PasswordSizeInBytes, UTF8_PasswordSize, CopyOfPasswordInByteAndSalt[UTF8_PasswordSize]);
					pshk_log_write_w(h, buffer_sprintf);
					_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"DEBUG : begin of UTF8 encoded password = %02hhX%02hhX %02hhX%02hhX %02hhX%02hhX %02hhX%02hhX %02hhX%02hhX %02hhX%02hhX %02hhX%02hhX",
						(char)CopyOfPasswordInByteAndSalt[0x0], (char)CopyOfPasswordInByteAndSalt[0x1], (char)CopyOfPasswordInByteAndSalt[0x2], (char)CopyOfPasswordInByteAndSalt[0x3], (char)CopyOfPasswordInByteAndSalt[0x4],
						(char)CopyOfPasswordInByteAndSalt[0x5], (char)CopyOfPasswordInByteAndSalt[0x6], (char)CopyOfPasswordInByteAndSalt[0x7], (char)CopyOfPasswordInByteAndSalt[0x8], (char)CopyOfPasswordInByteAndSalt[0x9],
						(char)CopyOfPasswordInByteAndSalt[0xA], (char)CopyOfPasswordInByteAndSalt[0xB], (char)CopyOfPasswordInByteAndSalt[0xC], (char)CopyOfPasswordInByteAndSalt[0xD]);
					pshk_log_write_w(h, buffer_sprintf);
				}
				// Update the password size with the right number of bytes
				PasswordSizeInBytes = UTF8_PasswordSize;
				if (PasswordSizeInBytes >= (PASSWORDSIZE - 4)) {
					PasswordSizeInBytes = PASSWORDSIZE - 4;
					pshk_log_write_a(h, "Warning : Truncated unicode password because of exeded size (>= 0x1FC bytes)");
				}
#endif	// UNICODEPASSWORDTOUTF8

				// -----------------
				// To be sure the SHA512 is calculate correctly, so we display the first 16 byte of it
				if (pshk_config.logLevel >= PSHK_LOG_ALL) {
#define SHA(a) sha512out[a]
					ret = sha512((unsigned char*)CopyOfPasswordInByteAndSalt, PasswordSizeInBytes, sha512out);
					_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"DEBUG : begin of not salted SHA512's password=%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x", SHA(0), SHA(1), SHA(2), SHA(3), SHA(4), SHA(5), SHA(6), SHA(7), SHA(8), SHA(9), SHA(10), SHA(11), SHA(12), SHA(13), SHA(14), SHA(15));
#undef SHA
					pshk_log_write_w(h, buffer_sprintf);
				}

				// -----------------
				// part that try to initiate the pseudo random generator, define the salt and copy to the end of the password
				for (int i = 0; i < SALTSIZE; i++) {
					// Generate a random not null byte with Entropy = 255^8 = 17 . 10^20
					salt[i] = ((rand() % 0xFF) + 1) & 0xFF; // don't want 0x00
					CopyOfPasswordInByteAndSalt[PasswordSizeInBytes + i] = salt[i];
					if (pshk_config.logLevel >= PSHK_LOG_ALL) {
						_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"DEBUG : Salt[%d]=%02hhX", i, salt[i]);
						pshk_log_write_w(h, buffer_sprintf);
					}
				}
				// Put Null byte at the end
				// No buffer overflow because PasswordSizeInBytes <= PASSWORDSIZE and array size is PASSWORDSIZE+SALTSIZE+1
				CopyOfPasswordInByteAndSalt[PasswordSizeInBytes + SALTSIZE] = 0x00;

				if (pshk_config.logLevel >= PSHK_LOG_ALL) {
#define SALT_IN_PWD(a) CopyOfPasswordInByteAndSalt[PasswordSizeInBytes + a]
					_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"DEBUG : Passwdord end (%02hhX%02hhX%02hhX) Salt= %02hhX%02hhX:...:%02hhX%02hhX", SALT_IN_PWD(-3), SALT_IN_PWD(-2), SALT_IN_PWD(-1), SALT_IN_PWD(0), SALT_IN_PWD(1), SALT_IN_PWD(6), SALT_IN_PWD(7));
					pshk_log_write_w(h, buffer_sprintf);
#undef SALT_IN_PWD
				}

				// -----------------
				// Calculate SHA512 of the salted password
				ret = sha512((unsigned char*)CopyOfPasswordInByteAndSalt, PasswordSizeInBytes + SALTSIZE, sha512out);
#ifdef UNICODEPASSWORDTOUTF8
				//free(CopyOfPasswordInByteAndSalt);
#endif
				if ((pshk_config.logLevel >= PSHK_LOG_ERROR) && (ret != 0))
					pshk_log_write_a(h, "Error : Can't calculate SHA512");

				// -----------------
				// no more need the clear version of the password
				//SecureZeroMemory(CopyOfPasswordInByteAndSalt, PASSWORDSIZE);
				// allocate some buffer
				membuff = (char *)calloc(sizeof(char), MYTEMPBUFFSIZE + 2);
				if (membuff == NULL)
					pshk_log_write_a(h, "Error : allocation memory for membuff");

				// temporary join SHA1512 and Salt
				memcpy_s(membuff, MYTEMPBUFFSIZE, sha512out, SHA512SIZE);
				memcpy_s(membuff + SHA512SIZE, MYTEMPBUFFSIZE, salt, SALTSIZE);

				// Encode in B64 the two joined data
				if (pshk_config.logLevel >= PSHK_LOG_ALL)
					pshk_log_write_a(h, "DEBUG : Base64 encoding");
				sha512outB64 = b64_encode((unsigned char*)membuff, SHA512SIZE + SALTSIZE);
				if ((pshk_config.logLevel >= PSHK_LOG_ERROR) && (sha512outB64 == NULL))
					pshk_log_write_a(h, "Error - Can't allocate momory for encoding to B64");

				// Fill Header "{SSHA512}"
				result_of_snprintf = _snprintf_s((char *)SaltedSHA512Output, SSHA512SIZE + 1, _TRUNCATE, "{SSHA512}%s", sha512outB64);
				if ((pshk_config.logLevel >= PSHK_LOG_ERROR) && (result_of_snprintf != SSHA512SIZE))
					pshk_log_write_a(h, "Error - Can't join strings {SSHA512} and Base64 data");

				//if (pshk_config.logLevel >= PSHK_LOG_ALL)
				//{
				//	_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"DEBUG : SHA512 text = %hs", sha512outB64);
				//	pshk_log_write_w(h, buffer_sprintf);
				//}

				// cleaning part ...
				if (membuff != NULL) {
					SecureZeroMemory(membuff, MYTEMPBUFFSIZE);
					free(membuff);
				}
				if (sha512outB64 != NULL) {
					//SecureZeroMemory(sha512outB64, SHA512SIZE);
					free(sha512outB64);
				}
				// Copy result to "passwordCopy" and convert to wchar_t
				if (pshk_config.logLevel >= PSHK_LOG_ALL)
					pshk_log_write_a(h, "DEBUG : Copy of SSHA512 in array passwordCopy");
				result_of_snprintf = _snwprintf_s(passwordCopy, SSHA512SIZE + 1, SSHA512SIZE, L"%hs", SaltedSHA512Output);

				if (pshk_config.logLevel >= PSHK_LOG_ALL) {
					_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"DEBUG : length=%d/%zd, passwordCopy= %s", result_of_snprintf, wcslen(passwordCopy), passwordCopy);
					pshk_log_write_w(h, buffer_sprintf);
					pshk_log_write_a(h, "DEBUG : End HASH code");
				}
				if (buffer_sprintf != NULL) {
					SecureZeroMemory(buffer_sprintf, MYBIGBUFFSIZE);
					free(buffer_sprintf);
				}

			}
			else { // Part that ouput Password in clear view
				// URL encode username and password
				if (pshk_config.urlencode == TRUE) {
#ifndef RAW_USERNAME
					usernameCopy2 = rawurlencode(usernameCopy);
					if (usernameCopy2 != NULL) {
						free(usernameCopy);
						usernameCopy = usernameCopy2;
					}
					else if (pshk_config.logLevel >= PSHK_LOG_ERROR)
						pshk_log_write_a(h, "Error URL encoding username");
#endif

					passwordCopy2 = rawurlencode(passwordCopy);
					if (passwordCopy2 != NULL) {
						SecureZeroMemory(passwordCopy, wcslen(passwordCopy) * sizeof(wchar_t));
						free(passwordCopy);
						passwordCopy = passwordCopy2;
					}
					else if (pshk_config.logLevel >= PSHK_LOG_ERROR)
						pshk_log_write_a(h, "Error URL encoding password");
			}

				// Double-quote username and password
				if (pshk_config.doublequote == TRUE) {
#ifndef RAW_USERNAME
					usernameCopy2 = doublequote(usernameCopy);
					if (usernameCopy2 != NULL) {
						free(usernameCopy);
						usernameCopy = usernameCopy2;
					}
					else if (pshk_config.logLevel >= PSHK_LOG_ERROR)
						pshk_log_write_a(h, "Error double-quoting username");
#endif
					passwordCopy2 = doublequote(passwordCopy);
					if (passwordCopy2 != NULL) {
						SecureZeroMemory(passwordCopy, wcslen(passwordCopy) * sizeof(wchar_t));
						free(passwordCopy);
						passwordCopy = passwordCopy2;
					}
					else if (pshk_config.logLevel >= PSHK_LOG_ERROR)
						pshk_log_write_a(h, "Error double-quoting password");
		}
	}

			// Calculate needed buffer size
			bufferChars = wcslen(usernameCopy) + wcslen(passwordCopy) + wcslen(prog) + wcslen(args) + 32;
			if (pshk_config.logLevel >= PSHK_LOG_ALL) {
				_snwprintf_s(buffer_sprintf, 0x400, 0x3FF, L"DEBUG : length of command line is = %lld", bufferChars);
				pshk_log_write_w(h, buffer_sprintf);
				pshk_log_write_a(h, "DEBUG : End HASH code");
			}
			bufferBytes = bufferChars * sizeof(wchar_t);
			buffer = (wchar_t *)calloc(1, bufferBytes);

			if (buffer != NULL) {
				// Memset is fine here since this is just initializing (not wiping)
				memset(&si, 0, sizeof(si));
				memset(&pi, 0, sizeof(pi));
				memset(&sa, 0, sizeof(sa));

				si.cb = sizeof(si);

				if (pshk_config.inheritParentHandles) {
					si.dwFlags |= STARTF_USESTDHANDLES;
					DuplicateHandle(GetCurrentProcess(), h, GetCurrentProcess(), (LPHANDLE)&(si.hStdOutput), 0, TRUE, DUPLICATE_SAME_ACCESS);
					DuplicateHandle(GetCurrentProcess(), h, GetCurrentProcess(), (LPHANDLE)&(si.hStdError), 0, TRUE, DUPLICATE_SAME_ACCESS);
					sa.nLength = sizeof(SECURITY_ATTRIBUTES);
					sa.bInheritHandle = TRUE;
				}

				// Log the commandline if we at DEBUG loglevel or higher
				if (pshk_config.logLevel >= PSHK_LOG_DEBUG) {
					_snwprintf_s(buffer, bufferChars, bufferChars - 1, bufferFormat, prog, args, usernameCopy, pshk_config.logLevel >= PSHK_LOG_ALL ? passwordCopy : L"<hidden>");
					pshk_log_write_w(h, buffer);
					SecureZeroMemory(buffer, bufferBytes);
				}

				_snwprintf_s(buffer, bufferChars, bufferChars - 1, bufferFormat, prog, args, usernameCopy, passwordCopy);

				// Wipe password and free copies
				SecureZeroMemory(passwordCopy, wcslen(passwordCopy) * sizeof(wchar_t));
				free(usernameCopy);
				free(passwordCopy);

				// Launch external program
				progRet = CreateProcess(prog, buffer, pshk_config.inheritParentHandles ? &sa : NULL, NULL, pshk_config.inheritParentHandles ? TRUE : FALSE, pshk_config.processCreateFlags, pshk_config.environment, pshk_config.workingDir, &si, &pi);

				// Wipe and free buffer
				SecureZeroMemory(buffer, bufferBytes);
				free(buffer);

				// If we fail and we care about printing errors then do it
				if (!progRet) {
					if (pshk_config.logLevel >= PSHK_LOG_ERROR) {
						wchar_t *fm_buf;
						FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&fm_buf, 0, NULL);
						pshk_log_write_w(h, fm_buf);
						LocalFree(fm_buf);
					}
					ret = PSHK_FAILURE;
				}
				else {
					// Wait for the process the alotted time
					if (pshk_config.logLevel >= PSHK_LOG_DEBUG)
						pshk_log_write_a(h, "Wait for the process the alotted time");
					progRet = WaitForSingleObject(pi.hProcess, wait);
					if (progRet == WAIT_FAILED && pshk_config.logLevel >= PSHK_LOG_ERROR) {
						pshk_log_write_a(h, "Wait failed for the last process");
					}
					else if (progRet == WAIT_TIMEOUT) {
						if ((option == PSHK_PRE_CHANGE && pshk_config.logLevel >= PSHK_LOG_ERROR) || (option == PSHK_POST_CHANGE && pshk_config.logLevel >= PSHK_LOG_DEBUG))
							pshk_log_write_a(h, "Wait timed out for the last process");
						if (option == PSHK_PRE_CHANGE)
							ret = PSHK_FAILURE;
					}

					if (ret == PSHK_SUCCESS) {
						// If this is a pre-change program, then we care about the
						// exit code of the process as well.
						if (pshk_config.logLevel >= PSHK_LOG_DEBUG)
							pshk_log_write_a(h, "Wait for the process is ended");
						if (option == PSHK_PRE_CHANGE) {
							// Return fail if we get an exit code other than 0 or GetExitCodeProcess() fails
							if (GetExitCodeProcess(pi.hProcess, &exitCode) == FALSE) {
								if (pshk_config.logLevel >= PSHK_LOG_ERROR)
									pshk_log_write_a(h, "Error while recieving error code from process");
								ret = PSHK_FAILURE;
							}
							else if (exitCode)
								ret = PSHK_FAILURE;
						}
					}
				}
				CloseHandle(pi.hProcess);
				CloseHandle(pi.hThread);
				if (pshk_config.inheritParentHandles) {
					CloseHandle(si.hStdOutput);
					CloseHandle(si.hStdError);
				}
			}
			else if (pshk_config.logLevel >= PSHK_LOG_ERROR)
				pshk_log_write_a(h, ALLOCATION_ERROR);
}
		else if (pshk_config.logLevel >= PSHK_LOG_ERROR)
			pshk_log_write_a(h, ALLOCATION_ERROR);
		// Close log
		pshk_log_close(h);
	}

	// Release mutex
	ReleaseMutex(hExecProgMutex);

	return ret;
}